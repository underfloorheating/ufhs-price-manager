<?php
class Ufhs_Pricemgr_Model_Pricing extends Mage_Core_Model_Abstract
{
    public function getCategoryProductsHtml($cat = null)
    {
        $data = $this->_getCategoryProducts($cat);
        return Mage::app()->getLayout()
        ->createBlock('pricemgr/pricingtable')
        ->setTemplate('pricemgr/pricingtablecontent.phtml')
        ->setData('prices',$data)
        ->toHtml();
    }

    private function _getCategoryProducts($cat)
    {
        $return = [];
        $products = Mage::getModel('pricemgr/indexer')->getCollection()->addFieldToFilter('cat_id',$cat)->getData();
        $check  = Mage::getModel('catalog/product')->getCollection()->getAllIds();

        foreach ($products as $index)  {
            $id = $index['prod_id'];
            if (in_array($id, $check)) {
                $product = Mage::getModel('catalog/product')->load($id);
                if ($product->getVisibility() == 1) {
                    $config = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->getId()) ?: Mage::getModel('bundle/product_type')->getParentIdsByChild($product->getId());
                    $link = Mage::getModel('catalog/product')->load($config[0])->getProductUrl();
                } else {
                    $link = $product->getProductUrl();
                }
                $return[] = [
                'id' => $id,
                'name' => $product->getData('name'),
                'gtin' => $product->getData('gtin'),
                'sku' => $index['sku'] ?: $product->getData('sku'),
                'rrp' => $product->getData('msrp'),
                'price' => $product->getFinalPrice(),
                'link' => $link
                ];
            }
        }
        return $return;
    }

    public function getCategories()
    {
        $collection = Mage::getModel('pricemgr/indexer')->getCollection()->addFieldToSelect('cat_id')->getData();
        $return = [];
        foreach ($collection as $entry) {
            if (!isset($return[$entry['cat_id']])) {
                $return[$entry['cat_id']] = $this->_getCategoryTextPath($entry['cat_id']);
            }
        }
        return $return;
    }

    public function getDefaultCat()
    {
        // Some backend admin ndoe needs to be in here, but if that hasnt been set them just take the first colleciton option
        // If that doesnt exist, fresh install, then zero it off
        return Mage::getModel('pricemgr/indexer')->getCollection()->getFirstItem()->getData('cat_id');
    }

    private function _getCategoryTextPath($catId)
    {
        $text = '';
        $category = Mage::getModel('catalog/category')->load($catId);
        $cats = explode('/',$category->getPath());
        foreach (array_slice($cats, 2) as $id) {
            $text .= Mage::getModel('catalog/category')->load($id)->getName() . ' > ';
        }
        return substr($text, 0, -3);
    }
}