<?php
class Ufhs_Pricemgr_Model_Resource_Indexer_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('pricemgr/indexer');
    }
}