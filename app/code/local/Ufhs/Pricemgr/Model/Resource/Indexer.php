<?php
class Ufhs_Pricemgr_Model_Resource_Indexer extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('pricemgr/indexer', 'id');
    }
}