<?php
$installer = $this;

$installer->startSetup();

$installer->run("
    DROP TABLE IF EXISTS {$installer->getTable('pricemgr/indexer')};

    CREATE TABLE {$installer->getTable('pricemgr/indexer')} (
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `prod_id` int(11) unsigned NOT NULL,
    `cat_id` int(11) unsigned NOT NULL,
    `sku` varchar(250) DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");

$installer->endSetup();