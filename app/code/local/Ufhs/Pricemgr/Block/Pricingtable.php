<?php
class Ufhs_Pricemgr_Block_Pricingtable extends Mage_Core_Block_Template
{
    public function getCategories()
    {
        return Mage::getModel('pricemgr/pricing')->getCategories();
    }

    public function getDefaultCat()
    {
        return Mage::getModel('pricemgr/pricing')->getDefaultCat();
    }

    public function getCategoryProductsHtml($cat = null)
    {
        $cat = $cat ?: $this->getDefaultCat();
        return Mage::getModel('pricemgr/pricing')->getCategoryProductsHtml($cat);
    }
}