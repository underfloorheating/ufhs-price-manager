<?php
class Ufhs_Pricemgr_Block_Adminhtml_Admintable extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('PricemgrAdminGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('pricemgr/indexer')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id',array(
            'header' => $this->__('ID'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'id'
            ));
        $this->addColumn('prodid',array(
            'header' => $this->__('Product ID'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'prod_id'
            ));
        $this->addColumn('prodname',array(
            'header' => $this->__('Product Name'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'prod_id'
            ));
        $this->addColumn('prodgtin',array(
            'header' => $this->__('Product GTIN'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'prod_id'
            ));
        $this->addColumn('cat',array(
            'header' => $this->__('Category'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'cat_id'
            ));
        $this->addColumn('sku',array(
            'header' => $this->__('ID'),
            'align' => 'left',
            'width' => '10px',
            'index' => 'sku'
            ));

        $object = new Varien_Object(array('grid_block' => $this));
        Mage::dispatchEvent("pricemgr_block_adminhtml_adintable_grid_preparecolumns", array("data" => $object));
        return parent::_prepareColumns();
    }

    public function getEmptyText()
    {
        return $this->__('No products have been added yet.');
    }

    public function getRowUrl($row)
    {
        return '/admin/pricemgr/editproduct/key/' . $row->getData()['id'];
    }
}