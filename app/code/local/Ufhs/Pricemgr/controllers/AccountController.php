<?php

class Ufhs_Pricemgr_AccountController extends Mage_Core_Controller_Front_Action
{
    /**
     * Pre-Dispach
     * -----------
     * Pre-dispatch authentication, courtesy of Alan Storm.
     */
    public function preDispatch()
    {
        parent::preDispatch();
        $action = $this->getRequest()->getActionName();
        $loginUrl = Mage::helper('customer')->getLoginUrl();

        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
        }
    }

    /**
     * Load Layout
     * -----------
     * Standardised Magento layout renderer.
     */
    private function _loadLayout()
    {
        $this->loadLayout();

        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('checkout/session');
        $this->_initLayoutMessages('catalog/session');
        $this->_initLayoutMessages('wishlist/session');

        $this->renderLayout();
    }

    /**
     * View Product Pricing Tables
     * ---------------------------
     */
    public function pricingAction()
    {
        $this->_loadLayout();
    }

    /**
     * Download Product Images
     * -----------------------
     */
    public function imagesAction()
    {
        $this->_loadLayout();
    }

    /**
     * Download Tier Pricelist
     * -----------------------
     */
    public function pricelistAction()
    {
        $this->_loadLayout();
    }

    public function pricingtableAction()
    {
        $post = Mage::app()->getRequest()->getParams();
        $cat = $post['cat'];
        echo Mage::getModel('pricemgr/pricing')->getCategoryProductsHtml($cat);
    }
}