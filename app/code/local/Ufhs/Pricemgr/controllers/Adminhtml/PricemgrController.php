<?php
class Ufhs_PriceMgr_Adminhtml_PricemgrController extends Mage_Adminhtml_Controller_action
{
    protected function _initAction()
    {
        $this->_title($this->__('Manual Lookup'));
        $this->loadLayout();
        $this->_initLayoutMessages('adminhtml/session');
        $this->_setActiveMenu('ufhs');
        return $this;
    }

    public function productadminAction()
    {
        $this->_initAction();
        $this->_title($this->__('Product Administration'));
        $this->renderLayout();
    }
}